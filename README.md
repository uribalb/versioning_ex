## Descritpion

Le programme suivant effectue les tâches classiques à effectuer avant une analyse de données en utilisant Gitlab CI/CD:
– Récupérer un jeu de données (https://raw.githubusercontent.com/ageron/handson-ml/master/datasets/housing/housing.csv)
– Nettoyer les données (enlever les colonnes avec trop de valeurs manquantes)
– Réduire les données (ne conserver que les variables numériques)
– Faire du feature Engineering (remplir les données manquantes avec les valeurs moyennes pour chaque variable et normaliser chaque variable)
– Afficher quelques statistiques sommaires relatives aux données (en utilisant la méthode "describe" de Pandas)

Pour éviter de rendre le fichier de config Gitlab CI inutilement compliqué, le préprocessing se déroule entièrement dans le programme main.py et le runner GitLab est uniquement chargé de créer l'image avec les dépendances appropriées.
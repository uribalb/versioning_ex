# Landry BETE, Master2IA

import pandas as pd
from sklearn.preprocessing import StandardScaler

def load_data(file_path):
    return pd.read_csv(file_path)

def remove_missing_columns(data, threshold=0.2):
    # Remove columns with more than 20% missing rows
    return data.dropna(thresh=len(data) * (1 - threshold), axis=1)

def remove_non_numerical_columns(data):
    # Remove non-numerical columns
    return data.select_dtypes(include='number')

def fill_missing_values(data):
    # Fill in missing values with the mean
    return data.fillna(data.mean())

def normalize_data(data):
    # Use StandardScaler to normalize the data
    scaler = StandardScaler()
    normalized_data = scaler.fit_transform(data)
    # Convert the normalized data back to a DataFrame
    return pd.DataFrame(normalized_data, columns=data.columns)


if __name__ == "__main__":
    file_path = "input_data.csv"
    data = load_data(file_path)
    
    # Data preprocessing steps
    data = remove_missing_columns(data)
    data = remove_non_numerical_columns(data)
    data = fill_missing_values(data)
    normalized_data = normalize_data(data)
    
    # Print summary statistics
    print(normalized_data.describe())
    
    # Save the processed data
    normalized_data.to_csv("processed_data.csv", index=False)
